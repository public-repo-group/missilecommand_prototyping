//
//  SKDraggingScene.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 18.11.22.
//

import SpriteKit
import Logging

import Assets
import Obstacles
import Controls
import MFIController

fileprivate let log = Log.logger()


class SKDraggingScene: SKScene, UIGestureRecognizerDelegate {

    enum Constants {
        static let spaceshipImage = "spaceship_360_360"
    }

    var previousXCoordinateInView: CGFloat? = nil
    var previousYCoordinateInView: CGFloat? = nil

    var fingerLocation: CGPoint? = nil
    var endLocation:  CGPoint? = nil
    var currentXLocation: CGFloat? = nil
    var panTranslation: CGPoint? = nil
    var parentViewSize: CGSize? = CGSize.zero

    var starfieldNode: SKSpriteNode? = nil
    var yAnchorPoint : CGFloat = -1.0

    var scrollAction: SKAction? = nil
    var waitAction: SKAction? = nil
    var actionSequence: SKAction? = nil

    var starFieldYPostion = CGFloat.zero
    var scrollBGNodeSize = CGSize.zero

    var spaceShip: SpaceShipSK? = nil
    var bullet: SKSimpleBullet? = nil


    var bullets: [Int: IProjectile] = Dictionary<Int, IProjectile>()

    var fireButton: FireControl?

//    var fireButtonTapRecognizer: UITapGestureRecognizer {
//        let tmp = UITapGestureRecognizer()
//        tmp.numberOfTapsRequired = 1
//        tmp.numberOfTouchesRequired = 1
//        return tmp
//    }

    var gameController = GameControllerDiscovery.instance
    var controlPanel: ControlPanel?

    var testBlock: SKObstacle? //= nil

    let moveLeftRightRecognizer: UIPanGestureRecognizer = {
        let tmp = UIPanGestureRecognizer()
        tmp.maximumNumberOfTouches = 1
        tmp.minimumNumberOfTouches = 1
        return tmp
    }()

    var obsticleManager = ObstacleManager.instance
    var obsticleAnimator = ObstacleAnimator.instance

    var viewNode: SKSpriteNode?

    required init?(coder: NSCoder) {
        log.info("| Entering init(coder:)")
        super.init(coder: coder)
        log.info("| Exiting init(coder:)")
    }

    override init(size: CGSize) {
        log.info("| Entering init() with size \(size)")
        super.init(size: size)
        let f = FontManager.Fonts.squareBlack.fontLarge

        gameController.startDiscovery()
        viewNode = SKSpriteNode(color: UIColor.clear, size: size)
        viewNode!.anchorPoint = CGPoint(x: 0, y: 0)
        currentXLocation = size.width / 2
        self.backgroundColor = UIColor.lightGray
        generateObsticle()
        log.info("| Exiting init()")
    }

    func generateObsticle() {
        testBlock = ObstacleBuilder.instance(self.size).createObsticle(id: 0, orientation: .HORIZONTAL_ORIENTATION)
        let xPos = CGFloat(ObstacleBuilder.instance(self.size).calculateHorizontalPos(forLength: testBlock!.children.count - 1))
        let calcPos = CGPoint(x: CGFloat(xPos * testBlock!.blockWidth),
                              y: (self.size.height - testBlock!.size.height))

        let testBlock2 = ObstacleBuilder.instance(self.size).createObsticle(id: 1, orientation: .HORIZONTAL_ORIENTATION)
        let xPos2 = CGFloat(ObstacleBuilder.instance(self.size).calculateHorizontalPos(forLength: testBlock2.children.count - 1))
        let calcPos2 = CGPoint(x: CGFloat(xPos2 * testBlock2.blockWidth),
                               y: (self.size.height - (testBlock2.size.height * 2.5)))

        let testBlock3 = ObstacleBuilder.instance(self.size).createObsticle(id: 2, orientation: .HORIZONTAL_ORIENTATION)
        let xPos3 = CGFloat(ObstacleBuilder.instance(self.size).calculateHorizontalPos(forLength: testBlock3.children.count - 1))
        let calcPos3 = CGPoint(x: CGFloat(xPos3 * testBlock3.blockWidth),
                               y: (self.size.height - (testBlock2.size.height * 1.0)))

        log.info("| Reference of obsticle \(testBlock!)")

        obsticleAnimator.addObsticle(obsticle: testBlock!)
        obsticleAnimator.setAnimationDescription(id: 0,
                                                 delta: 1.0 ,
                                                 forObsticle: testBlock!,
                                                 delay: TimeInterval(0.025))
        obsticleAnimator.setAnimationDescription(id: 1,
                                                 delta: 1.0 ,
                                                 forObsticle: testBlock2,
                                                 delay: TimeInterval(0.0125))

        obsticleAnimator.setAnimationDescription(id: 2,
                                                 delta: 1.0 ,
                                                 forObsticle: testBlock3,
                                                 delay: TimeInterval(0.0070))

        testBlock!.position = calcPos
        testBlock2.position = calcPos2
        testBlock3.position = calcPos3

    }

    func beganMovement() {
     //   let desc = obsticleAnimator.animationDescs[0]
     //   desc!.object.curPos.y = desc!.object.curPos.y - 10
    }

    @objc
    func handleSwipe(_ gesture: UIPanGestureRecognizer) {
      //  log.info("| Entering handleSwipe()")

        guard let view = gesture.view else { return }

        switch gesture.state {
            case .began:
                if endLocation == nil {
                    let location = gesture.location(in: view)
                    endLocation = CGPoint(x: self.size.width * 50 / 100, y: location.y)
                }
                log.info("| ===> Gesture starts at \(endLocation!.x)")
            case .changed:
                panTranslation = gesture.translation(in: view)
                let tmp = endLocation!.x + panTranslation!.x

                yAnchorPoint = panTranslation!.x
                log.info("| yAnchorPoint \(yAnchorPoint)")

                if yAnchorPoint >= self.view!.frame.size.height {
                    log.info("| yAnchorPoint is \(yAnchorPoint) is bigger than height \(self.view!.frame.size.height) , setting it to 0")
                    yAnchorPoint = 0
                 return
                }

                if  tmp >= parentViewSize!.width {
                    log.info("| Projected X-position \(tmp) exceeds width of view \(parentViewSize!.width) ")
                    spaceShip!.position.x = parentViewSize!.width
                    return
                }

                if tmp <= 0 {
                    log.info("| Projected X-position \(tmp) is below 0")
                    spaceShip!.position.x = 0
                    return
                }
                var pos = spaceShip!.position
                pos.x = tmp
                spaceShip!.position = pos
         case .ended:
                panTranslation = gesture.translation(in: view)
                let tmp = endLocation!.x + panTranslation!.x
                if (tmp > 0) && (tmp <= parentViewSize!.width) {
                    endLocation!.x = endLocation!.x + panTranslation!.x
                } else if tmp < 0 {
                    endLocation!.x = 0
                } else if tmp > parentViewSize!.width {
                    endLocation!.x = parentViewSize!.width
                }
                log.info("| ===> Gesture ended at X \(endLocation!.x)")
            default:
                log.info("| State is \(gesture.state) and of no interest")
        }
     //   log.info("| Exiting handleSwipe()")
    }

}

// MARK: Automatic background scrolling

extension SKDraggingScene {


    func scrollBackground() {
        log.info("| Entering scrollBackground()")


        log.info("| Exitign scrollBackground() ")

    }


}


// MARK: GestureRecognizer - Delegate

extension SKDraggingScene {
    override func didMove(to view: SKView) {
        moveLeftRightRecognizer.addTarget(self, action: #selector(handleSwipe(_:)))
        moveLeftRightRecognizer.delegate = self
        parentViewSize = self.view!.frame.size

        starfieldNode = SKStarfieldBackground(size: parentViewSize!, color: .clear)
        self.addChild(starfieldNode!)

        scrollBGNodeSize = starfieldNode!.size

        self.anchorPoint = CGPoint(x: 0.0, y: 0.0)
       // self.view!.addGestureRecognizer(moveLeftRightRecognizer)

       // setupScrolling()

      //  let img = EnumIcon.car

    //    let dsdsd = img.systemIconImage

     //   let fm = FontManager.Fonts.learnToFlyRegular.fontSmall
       fireButton = FireControl(position: CGPoint(x: 0, y: 0),
                                size: CGSize(width: parentViewSize!.width * 10 / 100,
                                             height: parentViewSize!.width * 10 / 100))



        let spaceShipSize = CGSize(width: parentViewSize!.width * 20 / 100,
                                   height: parentViewSize!.height * 20 / 100)

        let bulletSize = CGSize(width: spaceShipSize.width * 12.5 / 100,
                                height: spaceShipSize.height * 25 / 100 )

        let spaceShipPosition = CGPoint(x: parentViewSize!.width * 50/100 /*- ( parentViewSize!.width * 5 / 100)*/,
                                        y: ( spaceShipSize.height * 1 ))

        let bulletPosition = CGPoint(x: spaceShipPosition.x , y: (spaceShipPosition.y + spaceShipSize.height * 40 / 100 ))

        spaceShip = SpaceShipSK(size: spaceShipSize, position: spaceShipPosition)
        bullet = SKSimpleBullet(position: bulletPosition, size: bulletSize /*, moveDirection: .VERTICAL_UP*/ )

      //  self.addChild(testBlock!)
      //  self.addChild(obsticleAnimator.animationDescs[1]!.object)
      //  self.addChild(obsticleAnimator.animationDescs[2]!.object)
        self.addChild(spaceShip!)

        self.addChild(fireButton!)
        // Setup game scene
        fireButton!.setupAferAddingTo(self)
        self.view!.addGestureRecognizer(fireButton!.tapGestureRecognizer)
       // self.addChild(bullet!)

    }


    func setupScrolling() {

        waitAction = SKAction.wait(forDuration: 0.0125)

        scrollAction = SKAction.run {

            self.starFieldYPostion -= 1.0

            if self.starFieldYPostion <= 0 /*self.scrollBGNodeSize.height*/ {
                log.info("| Resetting position of background scrolling node to from \(self.starFieldYPostion) to 0")
                self.starFieldYPostion = self.scrollBGNodeSize.height
            }
            self.starfieldNode!.position = CGPoint(x: 0, y: self.starFieldYPostion)
          //  self.testBlock!.curPos.y = self.testBlock!.curPos.y - 1
        }

        actionSequence = SKAction.sequence([ waitAction!, scrollAction!])

        let desc = obsticleAnimator.animationDescs[0]
       // let action = desc!.actionSequence

        obsticleManager.runAnimation(id: 0, onNode: self, repeatForever: true)
        obsticleManager.runAnimation(id: 1, onNode: self, repeatForever: true)
        obsticleManager.runAnimation(id: 2, onNode: self, repeatForever: true)

        self.run(SKAction.repeatForever(actionSequence!))

    }
}


// MARK: Setup onscreen controls

extension SKDraggingScene {

    func setupFireControl() {
        log.info("| Entering setupFireControl()")


        log.info("| Exiting setupFireControl()")
    }




}
