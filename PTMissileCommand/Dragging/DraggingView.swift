//
//  DraggingView.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 18.11.22.
//

import SwiftUI
import SpriteKit

import Logging

struct DraggingView: View {

    var body: some View {

        GeometryReader { proxy in
            ZStack {
                SpriteView(scene: SKDraggingScene(size: proxy.size))
                    .frame(width: proxy.size.width,
                           height: proxy.size.height,
                           alignment: .topLeading)

            }.frame(width: proxy.size.width ,
                    height: proxy.size.height,
                    alignment: .topLeading)
            .ignoresSafeArea()
        }
    }
}



struct DraggingView_Previews: PreviewProvider {
    static var previews: some View {
        DraggingView()
    }
}
