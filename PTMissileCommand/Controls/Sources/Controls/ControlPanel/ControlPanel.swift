//
//  ControlPanel.swift
//  
//
//  Created by Frank Marx on 16.02.23.
//

import Foundation
import SpriteKit

import Logging


fileprivate let log = Log.logger()


public class ControlPanel: SKSpriteNode {

    var parentSize: CGSize


    public init(position: CGPoint, size: CGSize, parent: SKNode) {
        log.info("| Entering init()")
        parentSize = parent.frame.size
        super.init(texture: nil, color: UIColor.lightGray, size: size)
        log.info("| Exiting init()")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("Not implemented!")
    }


}

