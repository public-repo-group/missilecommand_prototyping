//
//  SpriteNodeLifecycle.swift
//  
//
//  Created by Frank Marx on 28.02.23.
//

import Foundation
import SpriteKit


@objc
public protocol ISpriteNodeLifecycle: AnyObject {

    @objc optional func setupAferAddingTo(_ parent: SKScene)

}
