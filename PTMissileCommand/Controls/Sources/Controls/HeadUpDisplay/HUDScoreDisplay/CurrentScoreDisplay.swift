//
//  CurrentScoreDisplay.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 29.01.23.
//

import Foundation
import Combine
import SpriteKit

import Logging


fileprivate let log = Log.logger()

public class CurrentScoreHUD: SKSpriteNode {

    enum FontSizesPoints {
        static let small = CGFloat(12)
        static let medium = CGFloat(24)
        static let large = CGFloat(48)
        static let extraLarge = CGFloat(96)
    }

    var currentScore = ""
    var fontSize: EnumFontSize = .medium

    enum EnumFontSize: String, CaseIterable, RawRepresentable {
        case small = "small"
        case medium = "medium"
        case large = "large"
        case extraLarge = "extraLarge"
    }


    public convenience init(parent: SKNode, initialValue: Int = 0, position: CGPoint, size: CGSize) {
        log.info("| Entering init() position \(position) , size \(size)")
        self.init(texture: nil, color: .clear, size: size)
        self.position = position
        log.info("| Exiting init()")
    }


}
