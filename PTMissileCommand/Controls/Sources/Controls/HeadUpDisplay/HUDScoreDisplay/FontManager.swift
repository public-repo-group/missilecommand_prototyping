//
//  FontManager.swift
//  
//
//  Created by Frank Marx on 01.02.23.
//

import Foundation
import UIKit
import SpriteKit
import SwiftUI

import Logging

fileprivate let log = Log.logger()

public class FontManager {

    enum FontSize {
        static let small = CGFloat(12)
        static let regular = CGFloat(24)
        static let medium = CGFloat(48)
        static let large = CGFloat(96)
        static let extraLarge = CGFloat(192)
    }


    public enum Fonts: String, CaseIterable, RawRepresentable {
        case learnToFlyRegular = "LearntoFly-Regular"
        case squareBlack = "Square-Black"

    public var fontSmall: Font {
            switch self {
                case .learnToFlyRegular:
                    let s = self.rawValue
                    let font = Font.custom(self.rawValue,
                                           size: FontSize.small)
                    return font
                case .squareBlack:
                    let font = Font.custom(self.rawValue,
                                           size: FontSize.small)
                    return font
            }
        }

    public var fontLarge: Font {
            switch self {
                case .learnToFlyRegular:
                    let font = Font.custom(self.rawValue,
                                           size: FontSize.large)
                    return font
                case .squareBlack:
                    let font = Font.custom(self.rawValue,
                                           size: FontSize.large)
                    return font
            }
        }
    }


    public static let instance: FontManager = {
        FontManager()
    }()

    private init() {
        log.info("| Entering init().")

        log.info("| Exiting init().")
    }




}

/*
 //
 //  CustomFonts.swift
 //  BurgerConstructionKit
 //
 //  Created by Frank Marx on 24.12.21.
 //

 import SwiftUI



 public let BurgerFont = "CheeseBurger"
 public let ChalkBoardOne = "ChalkBoardOne"
 public let FastFood = "fastfood"
 public let TenThousandReasons = "KG Ten Thousand Reasons"


 public class TenThousandReasonsFont {

     public static let large: Font = {
         Font.custom(TenThousandReasons, size: 240.0)
     }()

     public static let medium: Font = {
         let f = Font.custom(TenThousandReasons, size: 120.0)
         return f
     }()

     public static let regular: Font = {
         let f = Font.custom(TenThousandReasons, size: 60.0)
         return f
     }()

     public static let small: Font = {
         let f = Font.custom(TenThousandReasons, size: 30.0)
         return f
     }()

     public static func fontCustomSize(size: CGFloat) -> UIFont {
         let f = UIFont(name: TenThousandReasons, size: size)!
         return f
     }

 }

 public class CheeseburgerFont {
     public static let veryLarge: Font = {
         let f = Font.custom("CheeseBurger", size: 240.0)
         return f
     }()

     public static let large: Font = {
         let f = Font.custom("CheeseBurger", size: 120.0)
         return f
     }()

     public static let medium: Font = {
         let f = Font.custom("CheeseBurger", size: 60.0)
         return f
     }()

     public let small: Font = {
         let f = Font.custom("CheeseBurger", size: 30.0)
         return f
     }()

     public static func fontCustomSize(size: CGFloat) -> UIFont {
         let f = UIFont(name: "CheeseBurger", size: size)!
         return f
     }
 }

 public class ChalkboardFont {
     public static let large: Font = {
         let f = Font.custom(TenThousandReasons, size: 240.0)
         return f
     }()

     public static let medium: Font = {
         let f = Font.custom(TenThousandReasons, size: 120.0)
         return f
     }()

     public static let regular: Font = {
         let f = Font.custom(TenThousandReasons, size: 60.0)
         return f
     }()

     public static let small: Font = {
         let f = Font.custom(TenThousandReasons, size: 40.0)
         return f
     }()

 }

 public class FastfoodFont {
     public static let veryLarge: Font = {
         let f = Font.custom(FastFood, size: 240.0)
         return f
     }()

     public static let large: Font = {
         let f = Font.custom(FastFood, size: 160.0)
         return f
     }()

     public static let medium: Font = {
         let f = Font.custom(FastFood, size: 120.0)
         return f
     }()
 }

 /*
  .process("Assets/Fonts/ChalkboardOne.ttf") ,
  .process("Assets/Fonts/CheeseBurger.ttf") ,
  .process("Assets/Fonts/fastfood.ttf") ,
  .process("Assets/Fonts/KgTenThousandReasons-R1ll.ttf")
  */

 public enum CustomFonts: String, CaseIterable {
    // case ChalkBoardOne = "ChalkBoardOne"
     case CheeseBurger = "CheeseBurger"
     case Fastfood = "fastfood"
     case TenThousandReasons = "KgTenThousandReasons-R1ll"
 }

 public struct FontManager {

     public static func registerFonts() {
         CustomFonts.allCases.forEach { font in
             registerFont(bundle: .module, fontName: font.rawValue, fontExtension: "ttf")
         }

     }


     fileprivate static func registerFont(bundle: Bundle, fontName: String, fontExtension: String) {

         guard let fontURL = bundle.url(forResource: fontName, withExtension: fontExtension),
             let fontDataProvider = CGDataProvider(url: fontURL as CFURL),
             let font = CGFont(fontDataProvider) else {
                 fatalError("Couldn't create font from filename: \(fontName) with extension \(fontExtension)")
             }
         var error: Unmanaged<CFError>?
         CTFontManagerRegisterGraphicsFont(font, &error)

     }

 }








 */

