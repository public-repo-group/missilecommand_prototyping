//
//  File.swift
//  
//
//  Created by Frank Marx on 12.02.23.
//

import Foundation
import Combine
import SpriteKit
import UIKit

import Logging

fileprivate let log = Log.logger()

public class FireControl: SKSpriteNode {

    enum Constants {
        static let fireButtonTexture = "fire_button_texture"
    }

    var buttonTexture: SKTexture
    public var tapGestureRecognizer: UITapGestureRecognizer {
        let tmp = UITapGestureRecognizer()
        tmp.numberOfTapsRequired = 1
        tmp.numberOfTouchesRequired = 1
        tmp.isEnabled = true
        return tmp
    }


    private let fireEventSubject = PassthroughSubject<EnumFireEvent, Never>()

    var fireEventPublisher: AnyPublisher<EnumFireEvent, Never> {
        fireEventSubject.eraseToAnyPublisher()
    }

    public init(position: CGPoint, size: CGSize) {
        log.info("| Entering init() with pos \(position), size \(size)")

        let image = UIImage(named: Constants.fireButtonTexture, in: Bundle.module , compatibleWith: nil)!
        buttonTexture = SKTexture(image: image)
        super.init(texture: buttonTexture, color: UIColor.clear, size: size)
        self.position = position
        self.anchorPoint = CGPoint(x: 0, y: 0)
     //   self.inputView!.addGestureRecognizer(tapGestureRecognizer)

        log.info("| Exiting init()")
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


    @objc func fireSingleEvent(_ gesture: UITapGestureRecognizer) {
        log.info("| Entering fireSingleEvent()")
        fireEventSubject.send(EnumFireEvent.SINGLE_SHOT)
        log.info("| Exiting fireSingleEvent()")
    }

    func addFireEvenSubscriber(subscriber: any Subscriber<EnumFireEvent, Never>) {
        fireEventSubject.receive(subscriber: subscriber)
    }

    func removeAllSubscribers() {
        // TODO: Find out how to remove subscribers
        //  fireEventSubject.
    }

}


// MARK: Lifecycle - methods

extension FireControl: ISpriteNodeLifecycle {

//    override func didMove(to view: SKView) {
//
//    }


    public func setupAferAddingTo(_ parentScene: SKScene) {
        log.info("| Entering setupAferAddingTo()")

      //  let fr = v.frame


//        guard let pView = parent.scene?.view else {
//            log.info("| No inputView set to add gesture recognizer to")
//            return
//        }
        log.info("| Adding gesture recognizer to parent inputView")

//  .addTarget(self, action: #selector(handleSwipe(_:)))

        tapGestureRecognizer.addTarget(self, action: #selector(fireSingleEvent(_:)))
        tapGestureRecognizer.delegate = self

     //   parentScene.view!.addGestureRecognizer(tapGestureRecognizer)


        log.info("| Exiting setupAferAddingTo()")
    }


    func tappedButton() {
        log.info("| Entering tappedButton()")

        log.info("| Exiting tappedButton()")
    }

}

extension FireControl: UIGestureRecognizerDelegate {

    public func gestureRecognizerShouldBegin(_ recognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    

    public func gestureRecognizer(_ recognizer: UIGestureRecognizer, shouldReceive: UITouch) -> Bool {
        return true
    }

    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer ) -> Bool {
        return true
    }

}
