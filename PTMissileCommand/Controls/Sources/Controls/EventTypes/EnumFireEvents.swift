//
//  EnumFireEvents.swift
//  
//
//  Created by Frank Marx on 16.02.23.
//

import Foundation


enum EnumFireEvent: Int, CaseIterable, RawRepresentable {
    case SINGLE_SHOT = 1
    case DOUBLE_SHOT = 2
    case SIMPLE_BURST = 4
}
