//
//  GameControllerDiscovery.swift
//  
//
//  Created by Frank Marx on 19.02.23.
//

import Foundation
import GameController
import Combine

import Logging

fileprivate let log = Log.logger()

public class GameControllerDiscovery {


    private var cancelBag = Set<AnyCancellable>()

    var pubControllerDidConnect: AnyPublisher<NotificationCenter.Publisher.Output , NotificationCenter.Publisher.Failure>?
    var pubControllerDidBecomeCurrent: AnyPublisher<NotificationCenter.Publisher.Output , NotificationCenter.Publisher.Failure>?
    var pubControllerDidStopBeingCurrent: AnyPublisher<NotificationCenter.Publisher.Output , NotificationCenter.Publisher.Failure>?

    private var mfiController: GCController? = nil

//    var gameController: GCController? {
//        get {
//
//        }
//    }


    public static let instance: GameControllerDiscovery = {
        let tmp = GameControllerDiscovery()
        return tmp
    }()

    private init() {
        log.info("| Entering init().")
        log.info("| Exiting init().")
    }


    public func startDiscovery() {
        log.info("| Entering startDiscovery()")
        registerForGCNotifications()
        log.info("| Exiting startDiscovery()")
    }

    public func stopDiscovery() {
        log.info("| Entering stopDiscovery()")

        cancelBag.forEach { anyCancelable in
            anyCancelable.cancel()
        }

        cancelBag.removeAll()

        log.info("| Exiting stopDiscovery()")
    }

}

// MARK: Internal methods

extension GameControllerDiscovery {

    func queryForMFIGameController() ->  GCController? {
        log.info("| Entering queryForMFIGameController()")

        GCController.controllers().forEach { controller in
            log.info("| Checking controller \(controller.description)")

           // controller.
        }


        log.info("| Exiting queryForMFIGameController()")
        return nil
    }

    func registerForGCNotifications() {
        log.info("| Entering registerForGCNotifications()")

        let notificationCenter = NotificationCenter.default

       pubControllerDidConnect = notificationCenter.publisher(for: NSNotification.Name.GCControllerDidConnect)
                                                   .eraseToAnyPublisher()

       pubControllerDidBecomeCurrent = notificationCenter.publisher(for: NSNotification.Name.GCControllerDidBecomeCurrent)
                                                         .eraseToAnyPublisher()

       pubControllerDidStopBeingCurrent = notificationCenter.publisher(for: NSNotification.Name.GCControllerDidStopBeingCurrent)
                                                            .eraseToAnyPublisher()


        pubControllerDidConnect!.sink() { notification in
            log.info("| GCControllerDidConnect-Event: \(notification.description) for controller \(String(describing: (notification.object as! GCController).vendorName))")
            // Check for extended game pad (MFI-Controller)


        }.store(in: &cancelBag)

        pubControllerDidBecomeCurrent!.sink { notification in
            log.info("| GCControllerDidBecomeCurrent-Event: \(notification.description) for controller \(String(describing: (notification.object as! GCController).vendorName))")

            // Check for extended game pad (MFI-Controller)
            if let _ = ( notification.object as? GCController)?.extendedGamepad {
                log.info("| Current game controller HAS extended game pad, vendor \(String(describing: (notification.object as! GCController).vendorName))")
                self.mfiController = (notification.object as! GCController)
            } else {
                log.info("| Current game controller has NO extended game pad")
            }
        }.store(in: &cancelBag)

        pubControllerDidStopBeingCurrent!.sink { notification in
            log.info("| GCControllerDidStopBeingCurrent-Event: \(notification.description) for controller \(String(describing: (notification.object as!    GCController).vendorName))")
        }.store(in: &cancelBag)

        log.info("| Exiting registerForGCNotifications()")
    }


}
