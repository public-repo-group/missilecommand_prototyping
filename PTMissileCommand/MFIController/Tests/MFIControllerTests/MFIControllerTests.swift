import XCTest
@testable import MFIController

final class MFIControllerTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(MFIController().text, "Hello, World!")
    }
}
