//
//  SpaceShipSK.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 17.12.22.
//

import Foundation
import SpriteKit

import Logging

class SpaceShipSK: SKSpriteNode {

    enum Constants {
        static let spaceShipImage = "spaceship_360_360"
    }

    var spriteTexture: SKTexture = {
        let tmp = SKTexture(imageNamed: Constants.spaceShipImage)
        return tmp
    }()

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    init(size: CGSize, position: CGPoint) {
        super.init(texture: spriteTexture, color: .clear, size: size)
        self.position = position
        
    }


}
