//
//  ContentView.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 17.11.22.
//


import SwiftUI
import Logging

struct MainView: View {
    var body: some View {
        GeometryReader { proxy in
            VStack {
                DraggingView().ignoresSafeArea()
            }.frame(width: proxy.size.width,
                    height: proxy.size.height ,
                    alignment: .topLeading)
            .ignoresSafeArea()
        }

    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        MainView()
    }
}
