//
//  Test.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 23.02.23.
//

import Foundation
import SwiftUI
import UIKit
import Logging

fileprivate let log = Log.logger()

public enum EnumIcon: String, CaseIterable, RawRepresentable {
    case car = "car"
    case bus = "bus"
    case train = "tram"

    static var imageCache = Dictionary<EnumIcon.RawValue, Image>()
    var systemIconImage: Image {
        log.info("| Requesting image")
        switch self {
            case .car:
                if let image = EnumIcon.imageCache[self.rawValue] {
                    return image
                } else {
                    let carImage = Image(systemName: self.rawValue)
                    EnumIcon.imageCache[self.rawValue] = carImage
                    return carImage
                }
            case .bus:
                if let image = EnumIcon.imageCache[self.rawValue] {
                    return image
                } else {
                    let busImage = Image(systemName: self.rawValue)
                    EnumIcon.imageCache[self.rawValue] = busImage
                    return busImage
                }
            case .train:
                let trainImage = Image(systemName: self.rawValue)
                return trainImage
        }
    }
}



/*
 public enum Fonts: String, CaseIterable, RawRepresentable {
     case learnToFlyRegular = "LearntoFly-Regular"
     case squareBlack = "Square-Black"

 public var fontSmall: Font {
         switch self {
             case .learnToFlyRegular:
                 let font = Font.custom(self.rawValue,
                                        size: FontSize.small)
                 return font
             case .squareBlack:
                 let font = Font.custom(self.rawValue,
                                        size: FontSize.small)
                 return font
         }
     }
 */


//private var image: Image {
//private var iconName: String {
//switch transportType {
//case .automobile, .any:
//   // return Image(systemName: "car")
//    return "car"
//case .transit:
//  //  return Image(systemName: "bus")
//    return "bus"
//case .walking:
//  //  return Image(systemName: "figure.walk")
//    return "figure.walk"
//default:
//  //  return Image(systemName: "questionmark")
//    return "questionmark"
//}
//}
