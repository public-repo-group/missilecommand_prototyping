//
//  IProjectile.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 25.12.22.
//

import Foundation
import SpriteKit


enum EnumProjectileMoveDirection: Int, CaseIterable, RawRepresentable {
    case VERTICAL_UP = 1
    case VERTICAL_DOWN = 2
    case HORIZONTAL_LEFT = 4
    case HORIZONTAL_RIGHT = 8
}


struct ProjectileAnimationDesc {
    var id: String
    var moveDirection: EnumProjectileMoveDirection
    var deltaMovement: CGFloat
    var moveDelay: TimeInterval
}


protocol IProjectile: AnyObject {

    // func moveUpTillEndOfScreen()

    func moveUp()

    func moveDown()

    // func removeFromParent()

    func getParent() -> SKNode?

    //func isOffscreen() -> Bool

    var isOffscreen: Bool { get }

    var speed: CGFloat { get set }

  //  var moveDirection: EnumProjectileMoveDirection { get set }


}
