//
//  SKSimpleBullet.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 24.12.22.
//

import Foundation
import SpriteKit

import Logging


fileprivate let log = Log.logger()

class SKSimpleBullet: SKSpriteNode, IProjectile {

   // var moveDirection: EnumProjectileMoveDirection

    enum Constants {
        static let simpleBulletImageName = "SimpleBulletVertical_Transparent"
        static let verticalMoveSpeed: CGFloat = 0.0125
    }

    static let bulletTexture: SKTexture = {
        return SKTexture(imageNamed: Constants.simpleBulletImageName)
    }()

    var verticalDeltaDisplacement = CGFloat(1)

    let waitForVerticalMovement = SKAction.wait(forDuration: Constants.verticalMoveSpeed)


    convenience init(position: CGPoint, size: CGSize/*, moveDirection: EnumProjectileMoveDirection */) {
        self.init(position: position, size: size, color: UIColor.clear /*, moveDirection: moveDirection */ )
        //super.init(texture: SKSimpleBullet.bulletTexture, color: UIColor.clear, size: size)
        self.position = position
    }

    init(position: CGPoint, size: CGSize, color: UIColor /*moveDirection: EnumProjectileMoveDirection */) {
      //  self.moveDirection = moveDirection
        super.init(texture: nil, color: color, size: size)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

extension SKSimpleBullet {

    var isOffscreen: Bool {
        return true
    }

    func moveUp() {
        log.info("| Entering moveUp().")

        log.info("| Exiting moveUp().")
    }

    func moveDown() {
        log.info("| Entering moveDown().")

        log.info("| Exiting moveDown().")
    }

    func parentBoundaries() {

        guard let bounds = self.parent?.frame else {
            log.info("| SKSimpleBulletNode is has no parent yet.")
            return
        }
    }

    func getParent() -> SKNode? {
        return self.parent
    }


    func moveUpTillEndOfScreen() {

    }

}


// MARK: Move animations

extension SKSimpleBullet {






/*
 func setupScrolling() {

     waitAction = SKAction.wait(forDuration: 0.0125)

     scrollAction = SKAction.run {

         self.starFieldYPostion -= 1.0

         if self.starFieldYPostion <= 0 /*self.scrollBGNodeSize.height*/ {
             log.info("| Resetting position of background scrolling node to from \(self.starFieldYPostion) to 0")
             self.starFieldYPostion = self.scrollBGNodeSize.height
         }
         self.starfieldNode!.position = CGPoint(x: 0, y: self.starFieldYPostion)
     }

     actionSequence = SKAction.sequence([ waitAction!, scrollAction!])

     self.run(SKAction.repeatForever(actionSequence!))

 }
 */



}
