//
//  ProjectTileManager.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 07.02.23.
//

import Foundation
import SpriteKit

import Logging

fileprivate let log = Log.logger()


class ProjectileAnimationManager {

    enum Constants {
        static let maxProjectiles = 200
    }

    var projectilesCount = 0
    var currentProjectiles = Dictionary<Int, IProjectile>()

    static let instance: ProjectileAnimationManager = {
        ProjectileAnimationManager()
    }()


    private init() {
        log.info("| Entering init()")

        log.info("| Exiting init()")
    }

    func addProjectile(projectile: IProjectile) -> Bool {
        log.info("| Entering addProjectile()")

        if projectilesCount == (Constants.maxProjectiles - 1) {
            log.error("| Maximum projectiles count reached")
            return false
        }

        currentProjectiles[projectilesCount] = projectile
        projectilesCount += 1

        log.info("| Exiting addProjectile()")
        return true
    }


}
