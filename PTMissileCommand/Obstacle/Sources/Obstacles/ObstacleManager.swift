//
//  ObsticleManager.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 02.01.23.
//

import Foundation
import SpriteKit

import Logging

fileprivate let log = Log.logger()


public class ObstacleManager {

    var parentNode: SKSpriteNode?
    private var blocks = Dictionary<Int, SKObstacle>()

    private var animator: ObstacleAnimator

    public static let instance: ObstacleManager = {
        ObstacleManager()
    }()

    private init() {
        log.info("| Entering init().")
        animator = ObstacleAnimator.instance

        log.info("| Exiting init().")
    }


    @discardableResult
    public func addAnimationForObsticle(animationDesc: ObstacleAnimationDesc) -> Bool {
        log.info("| Entering addAnimationForObsticle() with id \(animationDesc.id)")
        let result = animator.addAnimationDescription(description: animationDesc)
        log.info("| Exiting addAnimationForObsticle()")
        return result
    }

    @discardableResult
    public func runAnimation(id: Int, onNode: SKNode, repeatForever: Bool = false ) -> Bool {
        log.info("| Entering runAnimation() with id \(id) with repeat forever \(repeatForever)")

        guard let animation = animator.getAnimation(withId: id) else {
            log.error("| Animation description with id \(id) not found")
            return false
        }

        if repeatForever {
            onNode.run(SKAction.repeatForever(animation.actionSequence))
        } else {
            onNode.run(animation.actionSequence)
        }
        log.info("| Exiting runAnimation()")
        return true
    }

    func addObsticle(key: Int, block: SKObstacle) -> Bool {
        log.info("| Entering addObsticle() with key \(key)")
        if let _ = blocks[key] {
            return false
        }
        blocks[key] = block
        log.info("| Exiting addObsticle()")
        return true
    }

}
