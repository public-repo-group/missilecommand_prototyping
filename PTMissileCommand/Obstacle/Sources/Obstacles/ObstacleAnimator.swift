//
//  ObsticleAnimator.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 04.01.23.
//

import Foundation
import SpriteKit

import Logging

fileprivate let log = Log.logger()


public class ObstacleAnimator {

    var obsticles = Dictionary<Int, SKObstacle>()
    var count = 0

    public var animationDescs = Dictionary<Int, ObstacleAnimationDesc>()

    public static let instance = {
        ObstacleAnimator()
    }()

    private init() {
        log.info("| Entering init().")

        log.info("| Exiting init().")
    }

    public func addObsticle(obsticle: SKObstacle) {
        log.info("| Entering addObsticle()")
        obsticles[count] = obsticle

        count += 1
        log.info("| Exiting addObsticle()")
    }

    public func getObsticles() -> [Int: SKObstacle] {
        return obsticles
    }

    public func removeObsticle(key: Int) -> SKObstacle? {
        log.info("| Entering removeObsticle() for key \(key)")
        guard let tmp = obsticles[key] else {
            log.info("| Obsticle with key \(key) not found.")
            return nil
        }
        obsticles.removeValue(forKey: key)
        log.info("| Exit removeObsticle()")
        return tmp
    }


    @discardableResult
    public func addAnimationDescription(description: ObstacleAnimationDesc) -> Bool {
        log.info("| Entering addAnimationDescription() with id \(description.id)")

        guard let _ = animationDescs[description.id] else {
            log.error("| Animation description with id \(description.id) already exists")
            return false
        }

        animationDescs[description.id] = description
        log.info("| Exiting addAnimationDescription()")
        return true
    }

    public func getAnimation(withId: Int) -> ObstacleAnimationDesc? {
        log.info("| Entering getAnimation() with id \(withId)")
        log.info("| Exiting getAnimaton()")
        return animationDescs[withId]
    }

    @discardableResult
    public func setAnimationDescription(id: Int, delta: CGFloat = CGFloat.zero,
                                 forObsticle: SKObstacle ,
                                 delay: TimeInterval) -> Bool {
        log.info("| Entering setAnimationDescription() with id \(id) and delta \(delta)")

//        guard let _ = animationDescs[id] else {
//            log.info("| Entry with id \(id) not found")
//            return false
//        }

        let waitAction = SKAction.wait(forDuration: delay)
        let obsticle = forObsticle
        let yMoveAction = SKAction.run {
            log.info("| Running y-move-action with obsticle \(obsticle)")
            obsticle.curPos.y -= delta
        }

        let actionSquence = SKAction.sequence([ waitAction, yMoveAction])

        let actionDesc = ObstacleAnimationDesc(id: id,
                                               object: obsticle,
                                               delayTimeInterval: delay,
                                               deltaY: delta,
                                               actionSequence: actionSquence)
        animationDescs[id] = actionDesc
        log.info("| Exiting setAnimationDescription()")
        return true
    }

    public func moveDown(id: Int, onParent: SKNode) {
        log.info("| Entering moveDown() with id \(id)")

        guard let desc = animationDescs[id] else {
            log.error("| No animation description with id \(id) found.")
            return
        }

      //  let obsticle = desc.object
        let animation = desc.actionSequence
        onParent.run(SKAction.repeatForever(animation))

        log.info("| Exiting moveDown()")
    }

    public func moveUp(id: Int) {
        log.info("| Entering moveUp) with id \(id)")

        log.info("| Exiting moveUp()")
    }

    public func stopAllMovements() {
        log.info("| Entering stopAllMovements()")
        log.info("| Exiting stopAllMovements()")
    }

    public func stopMovementFor(key: String) {
        log.info("| Entering stopMovementFor() with key \(key)")
        
        log.info("| Exiting stopMovementFor()")
    }

}


public class ObstacleAnimationDesc {
    private(set) public var id: Int
    private(set) public var object: SKObstacle
    private(set) public var delayTimeInterval: TimeInterval
    private(set) public var deltaY: CGFloat
    private(set) public var actionSequence: SKAction


    public init(id: Int, object: SKObstacle, delayTimeInterval: TimeInterval, deltaY: CGFloat, actionSequence: SKAction) {
        self.id = id
        self.object = object
        self.delayTimeInterval = delayTimeInterval
        self.deltaY = deltaY
        self.actionSequence = actionSequence
    }

}
