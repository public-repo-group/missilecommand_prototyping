//
//  ObsticleBuilder.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 28.12.22.
//

import Foundation
import SpriteKit
import UIKit

import Logging

import Assets

fileprivate let log = Log.logger()

public class ObstacleBuilder {

    enum Constants {
        static let maxBlocksPerRow = 10
        static let valueKey = "value"
        static let numberOfBlocksKey = "numberOfBlocks"
        static let blockWidthKey = "blockWidth"
    }


    public static let instance = { (pSize: CGSize) in
        ObstacleBuilder(pSize: pSize)
    }

    var parentSize: CGSize = CGSize.zero

    private init(pSize: CGSize) {
        log.info("| Entering init()")
        parentSize = pSize
        log.info("| Exiting init()")
    }


    public func calculateHorizontalPos(forLength: Int ) -> Int {
        if forLength >= (Constants.maxBlocksPerRow) {
            return 0
        }

        let remainingPositions = (Constants.maxBlocksPerRow ) - (forLength )
        let randomPosition = Int.random(in: 0...(remainingPositions - 1))

        return randomPosition
    }


    public func createObsticle(id: Int, orientation: ObstacleOrientationEnum, length: Int = 5) -> SKObstacle {
        log.info("| Entering createObsticle() with orientation \(orientation) and length \(length)")

        let randomLengthOfObsticle = Int.random(in: 1...Constants.maxBlocksPerRow)

        let randoms = generateRandomSeries(length: randomLengthOfObsticle)
        let blocks = assembleObsticleNode(randoms: randoms, orientation: orientation)

        let obsticle = SKObstacle.createObsticle(id: id,
                                                 parentFrame: CGRect(x: 0, y: 0, width: parentSize.width, height: parentSize.height),
                                                 blocks: blocks)
        log.info("| Exiting createObsticle()")
        return obsticle
    }

    func assembleObsticleNode(randoms: Array<String> , orientation: ObstacleOrientationEnum) -> Dictionary<Int, SKSpriteNode> {
        log.info("| Entering assembleObsticleNode()")

        var blockCount = 0

        let blockWidth = parentSize.width / CGFloat(Constants.maxBlocksPerRow)
        let blockHeight = blockWidth
        var blocks = Dictionary<Int, SKSpriteNode>()
        var xPosBlock = CGFloat(0)
        let yPosBlock = CGFloat(0)

        for asset in randoms {
            xPosBlock = CGFloat(blockWidth) * CGFloat(blockCount)
            let tmp = BlockAssetsConstantsEnum(rawValue: asset)!.texture
            let node = SKSpriteNode(texture: tmp)
            node.size = CGSize(width: blockWidth, height: blockHeight)
            node.anchorPoint = CGPoint(x: 0, y: 0)
            node.name = String(describing: blockCount)

            node.position = CGPoint(x: xPosBlock, y: yPosBlock)

            blocks[blockCount] = node
            blockCount += 1
        }

        log.info("| Exiting assembleObsticleNode()")
        return blocks
    }


    func generateRandomSeries(length: Int) -> Array<String> {
          log.info("| Entering generateRandomSeries() with length \(length)")
        var randoms = Array<String>()
        let cases = BlockAssetsConstantsEnum.allCases

        for _ in 0..<length {
            let r = Int.random(in: 0...9)
            let assetIdentidier = cases[r].rawValue
            randoms.append(assetIdentidier)
        }

        log.info("| Exiting generateRandomSeries()")
        return randoms
    }

}
