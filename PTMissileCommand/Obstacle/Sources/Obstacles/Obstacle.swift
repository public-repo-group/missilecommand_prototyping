//
//  Obsticle.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 28.12.22.
//


 // TODO: Rename all the "obsticle" to "obstacle"

import Foundation
import SpriteKit

import Logging

import Assets

fileprivate let log = Log.logger()


public enum ObstacleOrientationEnum: Int, RawRepresentable {
   case VERTICAL_ORIENTATION = 0
   case HORIZONTAL_ORIENTATION = 1
}

public class SKObstacle: SKSpriteNode {

    enum Constants {
        static let maxBlocks = 10
    }

    var id: Int = 0

    public var obsticleTextures = Array<SKTexture>()
    public var obsticleNodes = Array<SKSpriteNode>()

    public var blockHeight = CGFloat.zero
    public var blockWidth = CGFloat.zero

    public var numberOfBlocks = 0
    public var parentFrame: CGRect = CGRect.zero

    var blocks = Dictionary<Int, SKSpriteNode>()
    var observer: IObsticleObserver? = nil

    var isOffScreen = false

    open override var position: CGPoint {
        didSet {
            log.info("| Entering didSet() with value \(self.position)")

            // Calcuclate if block is already offscreen vertically
            let parentHeight = parentFrame.height
            let obsticleUpperYPos = self.position.y + blockHeight

            if obsticleUpperYPos < 0 && !isOffScreen {
                isOffScreen = true
                log.info("| Obsticle with id \(id) moved vertically off screen, y-Pos is \(self.position.y)")
            }

            log.info("| Exiting didSet()")
        }
    }

    public var curPos: CGPoint {
        get {
            return self.position
        }
        set(v) {
            self.position = v
            // Calcuclate if block is already offscreen vertically
        }
    }


    public static func createObsticle(id: Int,
                               parentFrame: CGRect, orientation: ObstacleOrientationEnum = .HORIZONTAL_ORIENTATION,
                               blocks: Dictionary<Int, SKSpriteNode>//,
                               /*observer: IObsticleObserver? = nil*/) -> SKObstacle {
        let inst = SKObstacle(id: id, parentFrame: parentFrame, blocks: blocks)
        inst.anchorPoint = CGPoint(x: 0, y: 0)
        inst.name = "ObsticleFrame"
        inst.color = UIColor.clear

        for block in blocks.values {
            log.info("| Position of block: \(block.position)")
            inst.addChild(block)
        }

        inst.blockWidth = parentFrame.width / CGFloat(Constants.maxBlocks)
        inst.blockHeight = inst.blockWidth

        let size = CGSize(width: (parentFrame.width / CGFloat(Constants.maxBlocks)) * CGFloat(inst.numberOfBlocks),
                          height: parentFrame.width / CGFloat(Constants.maxBlocks))
        inst.size = size
      //  inst.observer = observer

        return inst
    }

    private convenience init(id: Int, parentFrame: CGRect, blocks: Dictionary<Int, SKSpriteNode>) {
        self.init(color: .clear, size: parentFrame.size)
        self.id = id
        self.blocks = blocks
        self.parentFrame = parentFrame
    }

    public var isOnScreen: Bool {
        get {
            return true
        }
    }

}

extension SKObstacle {

    public override func addChild(_ node: SKNode) {
        if !(node is SKSpriteNode) {
            return
        }

        super.addChild(node)
        self.blocks[numberOfBlocks] = (node as! SKSpriteNode)
        numberOfBlocks += 1
    }

    func generateRandomSeries(length: Int) -> Array<String> {
        log.info("| Entering generateRandomSeries() with length \(length)")
        var randoms = Array<String>()
        let cases = BlockAssetsConstantsEnum.allCases

        for _ in 0..<length {
            let r = Int.random(in: 0...9)
            let assetIdentidier = cases[r].rawValue
            randoms.append(assetIdentidier)
        }

        log.info("| Exiting generateRandomSeries()")
        return randoms
    }

    func assembleObsticle() {
        log.info("| Entering assembleObsticle()")


        log.info("| Exiting assembleObsticle()")
    }


   // override func did


}


protocol IObsticleObserver: Any {

    func didMoveOffscreen(node: SKNode)

}


