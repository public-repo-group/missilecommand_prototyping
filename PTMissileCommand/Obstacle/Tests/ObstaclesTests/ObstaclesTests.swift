import XCTest
@testable import Obstacle

final class ObstaclesTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(Obstacle().text, "Hello, World!")
    }
}
