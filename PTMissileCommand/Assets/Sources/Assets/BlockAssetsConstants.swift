//
//  BlockAssetsConstants.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 28.12.22.
//

import Foundation
import SpriteKit



public enum BlockAssetsConstantsEnum:  String, CaseIterable {
    case big_0 = "0_big"
    case big_1 = "1_big"
    case big_2 = "2_big"
    case big_3 = "3_big"
    case big_4 = "4_big"
    case big_5 = "5_big"
    case big_6 = "6_big"
    case big_7 = "7_big"
    case big_8 = "8_big"
    case big_9 = "9_big"


    public var texture: SKTexture {
        switch self {
            case .big_0:
                let image = UIImage(named: "0_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_1:
                let image = UIImage(named: "1_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_2:
                let image = UIImage(named: "2_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_3:
                let image = UIImage(named: "3_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_4:
                let image = UIImage(named: "4_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_5:
                let image = UIImage(named: "5_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_6:
                let image = UIImage(named: "6_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_7:
                let image = UIImage(named: "7_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_8:
                let image = UIImage(named: "8_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
            case .big_9:
                let image = UIImage(named: "9_big", in: Bundle.module, compatibleWith: nil)
                return SKTexture(image: image!)
        }
    }
}
