//
//  SKStarfieldBackground.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 04.12.22.
//

import Foundation
import SpriteKit

import Logging

fileprivate let log = Log.logger()

class SKStarfieldBackground: SKSpriteNode {

    enum Constants {
        static let starfieldImageHorizontal = "starfield_bg_horizontal"
        static let starfieldImageVertical = "starfield_bg_vertical"
        static let starfieldBackgroundEndless = "starfield_endless_512_512"
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    var backgroundNode: SKSpriteNode?
    var upperTexture: SKSpriteNode?
    var lowerTexture: SKSpriteNode?

    init(size: CGSize, color: UIColor) {
        log.info("| Enterin init() with size \(size) and color \(color)")
        let calcSize = CGSize(width: size.width, height: size.height * 200/100)
        super.init(texture: nil,
                   color: color ,
                   size: calcSize)

        backgroundNode = SKSpriteNode(color: .clear, size: calcSize)

        setup()

        log.info("| Exiting init()")
    }

    private func setup() {
       let tmp = SKTexture(image: UIImage(named: Constants.starfieldBackgroundEndless)!)

        upperTexture = SKSpriteNode(color: .clear,
                                    size: CGSize(width: self.size.width,
                                                 height: self.size.height * 100 / 100))

       lowerTexture = SKSpriteNode(color: .clear,
                                   size: CGSize(width: self.size.width,
                                                height: self.size.height * 100 / 100))


       lowerTexture!.anchorPoint = CGPoint(x: 0, y: 0)
       upperTexture!.anchorPoint = CGPoint(x: 0.0 , y: 1.0)


       upperTexture!.texture = tmp
       lowerTexture!.texture = tmp

        self.addChild(lowerTexture!)
        self.addChild(upperTexture!)
    }

}
