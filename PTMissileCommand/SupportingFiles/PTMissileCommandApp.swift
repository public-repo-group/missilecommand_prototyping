//
//  PTMissileCommandApp.swift
//  PTMissileCommand
//
//  Created by Frank Marx on 17.11.22.
//

import SwiftUI

@main
struct PTMissileCommandApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
